
def pdb_scop_closest_domains_full_n_neighbors_post(nNeighbors, file, chain = None, segments = None, distanceCutoff = None) -> str:
    return 'do some magic!'

def pdb_scop_closest_domains_lite_n_neighbors_post(nNeighbors, file, chain = None, segments = None, distanceCutoff = None) -> str:
    return 'do some magic!'

def pdb_scop_nearest_domains_full_n_neighbors_post(nNeighbors, file, chain = None, segments = None, distanceCutoff = None) -> str:
    return 'do some magic!'

def pdb_scop_nearest_domains_lite_n_neighbors_post(nNeighbors, file, chain = None, segments = None, distanceCutoff = None) -> str:
    return 'do some magic!'

def vector_scop_closest_domains_full_n_neighbors_post(nNeighbors, foldSpaceVector) -> str:
    return 'do some magic!'

def vector_scop_closest_domains_lite_n_neighbors_post(nNeighbors, foldSpaceVector) -> str:
    return 'do some magic!'

def vector_scop_nearest_domains_full_n_neighbors_post(nNeighbors, foldSpaceVector) -> str:
    return 'do some magic!'

def vector_scop_nearest_domains_lite_n_neighbors_post(nNeighbors, foldSpaceVector) -> str:
    return 'do some magic!'
