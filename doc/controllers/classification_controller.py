
def pdb_cath_protein_structure_classification_classification_level_post(classificationLevel, file, chain = None, segments = None, distanceCutoff = None) -> str:
    return 'do some magic!'

def pdb_cath_protein_structure_classification_proba_classification_level_post(classificationLevel, file, chain = None, segments = None, distanceCutoff = None) -> str:
    return 'do some magic!'

def vector_cath_protein_structure_classification_classification_level_post(classificationLevel, foldSpaceVector) -> str:
    return 'do some magic!'

def vector_cath_protein_structure_classification_proba_classification_level_post(classificationLevel, foldSpaceVector) -> str:
    return 'do some magic!'

def vector_cath_protein_structure_classification_proba_post(foldSpaceVector) -> str:
    return 'do some magic!'
