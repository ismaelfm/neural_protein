#!/usr/bin/env python3

import os

import connexion

#if __name__ == '__main__':
app = connexion.App(__name__, specification_dir='./swagger/')
app.add_api(
    'swagger.yaml',
    arguments={
        'title':
        'Ultra fast protein structure classification and neighbor structure retrieval based on Residue Cluster Classes (RCCs) powered by  Deep Learning. See http://www.sciencedirect.com/science/article/pii/S147692711530092X',
        'host':
        os.getenv('NEURAL_PROTEIN_HOST')
    })
if __name__ == '__main___':
    app.run(port=8080)
