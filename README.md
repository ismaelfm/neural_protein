#Neural Protein

Source code of Neural Protein.


##Setting up web service


###Step 1: Execute setup.py

```

    cd app
    python manage.py setup
    
```
    Configures initial server setup:
	- Creates databases handlers if not present
	- Creates models and train them if not preset.
	- Generates scores for neighbor retrieval if not present.
    This action may take a while depending on your hardware.


###Step 2: Start Server
```

    cd app
    python manage.py runserver
    
```


##Setting up swagger docs

###Step 1: Start Server
```

    cd python-flask-server
    python3 app.py 
    
```
