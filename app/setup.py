import cPickle as pickle
import logging
import os

import numpy as np
from flask import current_app
from pymongo import MongoClient

from neuralprotein.static import loader
from rcc.RCCTools.space import FoldSpaceRepresentation
from rcc.RCCTools.structure_databases import FOLD_SPACE_DIM


def serialize_data(identifier, data):

    filename = identifier + '.pickle'
    filepath = os.path.join(current_app.config.get('DATA_PATH'), filename)
    logging.info('\tSerializing ' + identifier + ' data in ' + filepath)

    with open(filepath, 'wb') as f:
        save = {identifier: data}
        pickle.dump(save, f, pickle.HIGHEST_PROTOCOL)


def test_dir(path):
    if not os.path.exists(path):
        logging.info('Creating ' + path + ' dir...\n')
        os.makedirs(path)


def test_handler(identifier, handler_fun, configuration, init=False):

    if not init:
        logging.info('Loading ' + identifier + ' handler...')
        try:
            return loader.load_data(identifier)
        except IOError:
            logging.info('\tLoad failed.')
    logging.info('Initializing ' + identifier + ' handler...')
    handler = handler_fun(**configuration)
    serialize_data(identifier, handler)
    return handler


def test_space_representation(identifier,
                              handler,
                              level,
                              level_label,
                              n_representants,
                              init=False):
    if not init:
        try:
            logging.info('Loading ' + level + ' space representation...')
            return loader.load_space(identifier)
        except IOError:
            logging.info('\tLoad failed.')
    logging.info('Initializing ' + level + ' space representation...')
    space_representation = FoldSpaceRepresentation(handler, level, level_label,
                                                   n_representants)
    identifier_space = '%s%s' % (identifier,
                                 current_app.config.get('SPACE_STR'))
    serialize_data(identifier_space, space_representation)

    return test_space_representation(identifier, handler, level, level_label,
                                     n_representants)


def test_superfamily_scores():
    try:
        logging.info('Loading superfamily scores...')
        return loader.load_scores('superfamily')
    except IOError:
        logging.info('\tLoad failed.')
