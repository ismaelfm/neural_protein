import os

SECRET_KEY = os.environ.get('SECRET_KEY')

FILE_ARG_STR = 'file'
UPLOAD_FOLDER = 'tmp/'
ALLOWED_EXTENSIONS = set(['pdb'])

DATA_PATH = 'neuralprotein/static/data/'
DATABASES_PATH = 'neuralprotein/static/databases/'

MONGOLOG_URI = os.environ.get('MONGOLOG_URI')
MONGODATA_URI = os.environ.get('MONGODATA_URI')

LOGS_COLLECTION = 'logs'
SCOP_COLLECTION = 'scop'

SPACE_STR = '_space_rep'
N_REPRESENTANTS = 1

CATH_STR = 'cath'
CATH_SPACE_REPRESENTATION = 'CAT'
CATH_SPACE_LABEL = 'CA'
CATH_CONFIG = {
    'vectors_filename': os.path.join(DATABASES_PATH, 'CATHFINAL.txt'),
    'vectors_delimiter': ',',
    'sepchar': '_',
    'levels': ['C', 'CA', 'CAT'],
    'names_filename': os.path.join(DATABASES_PATH, 'CathNames.txt'),
    'names_delimiter': '|'
}

SCOP_STR = 'scop'
SCOP_CONFIG = {
    'vectors_filename': os.path.join(DATABASES_PATH, 'SCOPVectors.txt'),
    'vectors_delimiter': ',',
    'sepchar': '.',
    'levels': ['superfamily'],
    'names_filename': os.path.join(DATABASES_PATH, 'SCOPNames.txt'),
    'names_delimiter': '|'
}

REGISTERED_HANDLERS = [CATH_STR, SCOP_STR]
REGISTERED_SPACES = [CATH_STR]
REGISTERED_KDTREES = [SCOP_STR]

REGISTERED_SCORES = ['superfamily']

CATH_SERVICE_HOST = os.environ.get('CATH_SERVICE_HOST')
CATH_SERVICE_PORT = os.environ.get('CATH_SERVICE_PORT')

SCOP_SERVICE_HOST = os.environ.get('SCOP_SERVICE_HOST')
SCOP_SERVICE_PORT = os.environ.get('SCOP_SERVICE_PORT')

TIMEOUT = 5.0
