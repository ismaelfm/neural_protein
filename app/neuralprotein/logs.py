import functools
from datetime import datetime

from flask import current_app, request

from geoip import geolite2
from neuralprotein import mongo_log


def ip_log(func):
    @functools.wraps(func)
    def store_log(*args, **kwargs):
        ip = str(request.remote_addr)
        match = geolite2.lookup(ip)
        if match:
            mongo_log.db[current_app.config.get('LOGS_COLLECTION')].insert_one(
                {
                    'endpoint': request.url_rule.rule,
                    'headers': request.headers.items(),
                    'country': match.country,
                    'timestamp': datetime.now(),
                    'ip': ip,
                })

        return func(*args, **kwargs)

    return store_log
