import json
import os

import numpy as np
from flask import current_app

from neuralprotein import mongo_data
from serving import OUTPUT_STR, _classification_aux, _format_probas
from valid_functions import validate_n_neighbors

SCOP_URL = 'scop.berkeley.edu'


def _get_idx_domain(fsv=None):
    probas_message = _classification_aux(
        fsv=fsv,
        database_name=current_app.config.get('SCOP_STR'),
        signature='superfamily')[OUTPUT_STR]
    return _format_probas(probas_message).argmax()


def get_domain_urls(domain_id=None):
    '''
    Returns the url of the domain specified.

    # Arguments
        domain_id: int, id of the domain.

    # Returns
        url of the domain.
    '''
    urls = [
        '%s/sunid=%s' % (SCOP_URL, domain_id),
        'supfam.org/SUPERFAMILY/cgi-bin/scop.cgi?sunid=%s' % domain_id
    ]
    return urls


def get_domains_images(domains=None):
    '''
    Returns the urls of images of the domains specified.

    # Arguments
        domains: array-like, domains.

    # Returns
        List of dicts, each dict contains the keys: 'domain' and 'imageUrls'.
    '''
    cursor = mongo_data.db[current_app.config.get('SCOP_COLLECTION')].find({
        'domain': {
            '$in': domains
        }
    })

    return {
        document.get('domain'): document.get('imageUrls')
        for document in cursor
    }


def get_domains_info(domains=None,
                     metric_values=None,
                     metric_name=None,
                     full_description=None):
    '''
    Returns the info (domainId, name, metric, classification, externalLinks,
    imageUrls) of the domains specified.

    # Arguments
        domains: array-like, domains.
        metric_values: array-like, values of the metric used
        metric_name: str, name of the metric used.
        full_description: boolean, True if classification, externalLinks and
            imageUrls wanted to be present in the info, False otherwise.

    # Returns
        List dictionaries of the info o each domain. Keys present: domainId,
        name, metric, classification*, externalLinks*, imageUrls*. (*: optional)
    '''
    handlers = current_app.config.get('HANDLERS')
    scop_handler = handlers.get(current_app.config.get('SCOP_STR'))

    domains_data = scop_handler.get_domains_data(domains)
    domain_images = get_domains_images(domains) if full_description else []
    domains_info = []

    for idx, domain in enumerate(domains):
        info = {
            'domainId': domain,
            'name': '|'.join(domains_data[idx][1].split('!')),
            metric_name: float(metric_values[idx])
        }

        if full_description:
            info['classification'] = domains_data[idx][2]
            info['externalLinks'] = get_domain_urls(domains_data[idx][0])
            info['imageUrls'] = domain_images.get(domain)

        domains_info.append(info)

    return domains_info


def get_closest_domains(fold_space_vector=None, n=None, full_description=None):
    '''
    Returns the info (domainId, name, metric, classification, externalLinks,
    imageUrls) of the n closest domains of the RCC vector specified.

    # Arguments
        fold_space_vector: array-like, RCC vector.
        n: int, number of closest domains
        full_description: boolean, True if classification, externalLinks and
            imageUrls wanted to be present in the info, False otherwise.

    # Returns
        List dictionaries of the info o each domain. Keys present: domainId,
        name, metric, classification*, externalLinks*, imageUrls*. (*: optional)

    # Notes
        closest: similar to the PDB file.
    '''
    validate_n_neighbors(n)

    handlers = current_app.config.get('HANDLERS')
    scop_handler = handlers.get(current_app.config.get('SCOP_STR'))

    scores = current_app.config.get('SCORES')
    superfamily_scores = scores.get('superfamily')

    idx_domain = _get_idx_domain(fold_space_vector)
    structure_scores = superfamily_scores[:, idx_domain]

    neighbors_ids = structure_scores.argsort()[-n:][::-1]

    neighbors = [scop_handler.get_domains()[i] for i in neighbors_ids]
    scores_ = structure_scores[neighbors_ids]

    return get_domains_info(neighbors, scores_, 'score', full_description)


def get_nearest_domains(fold_space_vector=None, n=None, full_description=None):
    '''
    Returns the info (domainId, name, metric, classification, externalLinks,
    imageUrls) of the n nearest domains of the RCC vector specified.

    # Arguments
        fold_space_vector: array-like, RCC vector.
        n: int, number of closest domains
        full_description: boolean, True if classification, externalLinks and
            imageUrls wanted to be present in the info, False otherwise.

    # Returns
        List dictionaries of the info o each domain. Keys present: domainId,
        name, metric, classification*, externalLinks*, imageUrls*. (*: optional)

    # Notes
        nearest: near to the PDB file

    '''
    validate_n_neighbors(n)

    handlers = current_app.config.get('HANDLERS')
    scop_handler = handlers.get(current_app.config.get('SCOP_STR'))

    kdtrees = current_app.config.get('KDTREES')
    scop_kdtree = kdtrees.get(current_app.config.get('SCOP_STR'))

    (distances, positions) = scop_kdtree.query(fold_space_vector, k=n)

    if not hasattr(positions, '__iter__'):
        positions = [positions]  # When k is equal to 1, KDTREE.query() returns
        distances = [distances]  # single elements instead of iterables

    neighbors = [scop_handler.get_domains()[x] for x in positions]

    return get_domains_info(neighbors, distances, 'distance', full_description)
