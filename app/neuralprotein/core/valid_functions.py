import os
import time
import hashlib

from flask import abort, current_app
from webargs import ValidationError
from werkzeug import secure_filename

from rcc.RCCTools.structure_databases import (FOLD_SPACE_DIM,
                                                              cath_levels)


def get_hash_timestap():
    '''
    Returns a hash of the current time.

    # Returns
        A str of the hash.
    '''
    hash = hashlib.md5()
    hash.update(str(time.time()))

    return hash.hexdigest()


def allowed_file(filename=None):
    '''
    Checks if a file is allowed in the application.

    # Arguments
        filename: str, path of the file to be verified.

    # Return
        boolean, True if allowed, False otherwise.
    '''
    return '.' in filename and \
           filename.rsplit('.', 1)[1] in current_app.config.get('ALLOWED_EXTENSIONS')


def validate_file(f=None):
    '''
    Validator which succeeds if f is a allowed file in the application. If allowed
    saves the file in UPLOAD_FOLDER.

    # Arguments:
        f: str, path of the file to be validated.

    # Returns
        The path of the validated file.
    '''
    if not f or not allowed_file(f.filename):
        abort(415)

    secure_filename(f.filename)
    filename = get_hash_timestap()
    f.save(os.path.join(current_app.config['UPLOAD_FOLDER'], filename))

    pdb_file = current_app.config['UPLOAD_FOLDER'] + filename

    return pdb_file


def validate_n_neighbors(value=None):
    '''
    Validator which succeeds if value is positive.

    # Arguments:
        value: int, value to be validated.
    '''
    if value < 0:
        abort(400)
    return True


def must_be_list_of_pairs(list_=None):
    '''
    Validator which succeeds if list_ is a list of pairs, i.e. the list only have
    two elements.

    # Arguments:
        list_: list, to be validated.
    '''
    for value in list_:
        if (len(value) != 2):  #Verifiying pairs
            raise ValidationError('list not a list of pairs', 416)
    return True


def must_be_list_of_valid_range(list_=None):
    '''
    Validator which succeeds if list_ is a list of positive ascending elements
    that describesa range.

    # Arguments:
        list: list, to be validated.
    '''
    #Need to a list of positive ranges [a, b]
    for range_ in list_:
        if sorted(range_) != range_:  #Verifiying valid range
            raise ValidationError('not a valid range', 416)
        if range_[0] == range_[1]:
            raise ValidationError('not a valid range', 416)
        if (range_[0] < 0):
            raise ValidationError('not a valid range',  # Positive Range
                                  416)
    return True


def must_be_a_CATH_level(value=None):
    '''
    Validator which succeeds if value is a valid level of CATH, i.e. value is
    'C', 'CA', 'CAT' or 'CATH'.

    # Arguments:
        value: value to validated.
    '''
    if not value in cath_levels.keys():
        abort(400)
    return True


def must_be_positive(value=None):
    '''
    Validator which succeeds if value is positive.

    # Arguments:
        value: int, value to be validated.
    '''
    if value < 0:
        raise ValidationError('value not positive')
    return True


def must_be_single_character(str_=None):
    '''
    Validator which succeeds if str_ is a single characer.

    # Arguments:
        str_: str, value to be validated.
    '''
    if len(str_) != 1:
        raise ValidationError('not a single character')
    return True


def must_be_26_dim_vector(vector=None):
    '''
    Validator which succeeds if vector has FOLD_SPACE_DIM len.

    # Arguments:
        vector: array-like, vector to be validated.
    '''
    if len(vector) != FOLD_SPACE_DIM:  #Checking dimensions
        raise ValidationError('vector not 26 dim')
    return True
