import os

from flask import abort

from rcc.RIGpackage import RIGBuilder
from rcc.RIGpackage.RIGException import GraphException


def get_residue_interaction_graph(filename=None,
                                  chain=None,
                                  segments=None,
                                  distance_cutoff=None):
    '''
    Returns the residue interaction graph of the protein corresponding to the
    PDB file.

    #Arguments
        filename: str, path of PDB file.
        chain: str, chain to be consider, first chain present in PDB file by
            default.
        segments:  list of list of pairs of ints, chain segments to be consider.
        distance_cutoff: float, interaction threshold measured in Angstroms.
    '''
    try:
        rig = RIGBuilder.get_residue_interaction_graph(
            filename, chain, segments, distance_cutoff)
        os.remove(filename)  #Deleted file after processing
        return rig
    except GraphException:  #Empty graph
        os.remove(filename)
        abort(409)
    except KeyError:  #Invalid value for chain
        os.remove(filename)
        abort(406)


def get_residue_interaction_graph_json(filename=None,
                                       chain=None,
                                       segments=None,
                                       distance_cutoff=None):
    '''
    Returns the residue interaction graph of the protein corresponding to the
    PDB file, represented by a graph structures in JSON.

    # Notes
        https://github.com/jsongraph/json-graph-specification


    #Arguments
        filename: str, path of PDB file.
        chain: str, chain to be consider, first chain present in PDB file by
            default.
        segments:  list of list of pairs of ints, chain segments to be consider.
        distance_cutoff: float, interaction threshold measured in Angstroms.
    '''
    rig = get_residue_interaction_graph(filename, chain, segments,
                                        distance_cutoff)
    return RIGBuilder.rig_to_json(rig)
