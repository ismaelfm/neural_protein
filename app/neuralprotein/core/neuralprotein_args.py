import os
import gzip
import StringIO

from flask import current_app
from webargs import fields, validate

from valid_functions import (get_hash_timestap, must_be_26_dim_vector,
                             must_be_list_of_pairs, must_be_list_of_valid_range,
                             must_be_positive, must_be_single_character,
                             validate_file)
# arguments for the pdb endpoints
pdb_args = {
    'chain': fields.Str(missing=None,
                        validate=[must_be_single_character]),
    'segments': fields.List(
        fields.DelimitedList(fields.Int()),
        missing=[],
        validate=[must_be_list_of_pairs, must_be_list_of_valid_range]),
    'distance_cutoff': fields.Number(missing=5.0,
                                     validate=[must_be_positive]),
    '_gzip': fields.Str(missing=None,
                        load_from='Content-Encoding',
                        location='headers'),
}
# arguments for the vector endpoints
vector_args = {'foldSpaceVector':
    fields.DelimitedList(fields.Int(),
                         required=True,
                         validate=[must_be_26_dim_vector]),}


def uncompress_pdb_file(rawfile=None):
    '''
    Decompress a pdb file and saves it in UPLOAD_FOLDER with a unique name.

    # Arguments
        rawfile: regular file, a StringIO object, or any other object which
            simulates a pdb file.

    #Returns
        The path of the decompressed file.

    '''
    uncompressed = gzip.GzipFile(fileobj=rawfile, mode='rb')
    filename = get_hash_timestap()

    path = current_app.config['UPLOAD_FOLDER']

    with open(os.path.join(path, filename), 'wb') as f:
        f.write(uncompressed.read())

    return path + filename


def get_pdb_name(_gzip=None, rqst=None):
    '''
    Extracts the pdb file present in the rqst, saves it and returns the path
    where it was saved.

    # Arguments
        _gzip: boolean, True if file gzipped, False otherwise.
        rqst: Request object.
    '''
    return uncompress_pdb_file(StringIO.StringIO(
        rqst.data)) if _gzip else validate_file(rqst.files[
            current_app.config['FILE_ARG_STR']])
