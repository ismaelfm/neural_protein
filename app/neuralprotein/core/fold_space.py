import os

from flask import abort, current_app
from scipy.spatial import KDTree

from rcc.RCCpackage import RCCobject as rcco
from rcc.RCCpackage.RCCutils import getChains
from rcc.RIGpackage.RIGException import GraphException

point = lambda coordinates: {'x': coordinates[0], 'y': coordinates[1]}


def get_fold_space_vector(filename=None,
                          chain=None,
                          segments=None,
                          distance_cutoff=None):
    '''
    Returns a fold space vector representation of the protein corresponding to
    the PDB file.

    #Arguments
        filename: str, path of PDB file.
        chain: str, chain to be consider, first chain present in PDB file by
            default.
        segments:  list of list of pairs of ints, chain segments to be consider.
        distance_cutoff: float, interaction threshold measured in Angstroms.

    #Notes
        - Fold space representation used is based on Residue Cluster
            Classes (RCCs).
        - PDB file is deleted after processing.
    '''
    autochain = True if chain == None else False
    try:
        rccs = rcco.RCC(filename, chain, autochain, segments, distance_cutoff)
        os.remove(filename)  #Deleted file after processing
        return rccs.RCCvector
    except GraphException:  #Empty graph
        os.remove(filename)
        abort(409)
    except KeyError:  #Invalid value for chain
        os.remove(filename)
        abort(406)


def get_fold_space_vectors(filename=None, segments=None, distance_cutoff=None):
    chains = getChains(filename)
    vectors = []
    for chain in chains:
        try:
            rccs = rcco.RCC(
                pdb=filename,
                chain=chain,
                autochain=False,
                chain_segments=segments,
                distance_cutoff=distance_cutoff)
            vectors.append({'chain': chain, 'foldSpaceVector': rccs.RCCvector})
        except GraphException:  #Empty graph
            continue
        except KeyError:  #Invalid value for chain
            continue
    os.remove(filename)  #Deleted file after processing
    return vectors


def scale_fold_space_vectors(fold_space_vectors=None, database=None):
    '''
    Returns fold space vectors scaled to have zero mean and equal variance.

    #Arguments
        fold_space_vectors: array-like, sparse matrix, data to be scaled.

    #Returns
        Data centered and scaled.
    '''
    handlers = current_app.config.get('HANDLERS')
    #optimized to train neural networks
    return handlers[database].scale_rccs(fold_space_vectors)


def get_cath_space_representation():
    '''
    Returns 2d points corresponding to positions of the RCC fold space
    structures representats for visual representation.

    # Returns
        Dictionary containing keys: 'positions2d' and 'mapping'. Where
        'positions2d' is list of dictionaries with keys: 'x', 'y' and 'class',
        where x and y describes the position of the structure and class the
        encoded label. 'mapping' dictionary containing the mapping of the
        classes i to to normalized encoding.
    '''
    handlers = current_app.config.get('HANDLERS')
    cath_handler = handlers.get(current_app.config.get('CATH_STR'))

    space_reps = current_app.config.get('SPACE_REPS')
    cath_space = space_reps.get(current_app.config.get('CATH_STR'))

    coordinates, labels = cath_space.get_space_representation()

    lec = cath_handler.get_encoder(current_app.config.get('CATH_SPACE_LABEL'))
    encoded_labels = lec.transform(labels)

    positions2d = []
    for coor, label in zip(coordinates, encoded_labels):
        i = point(coor)
        i.update({'class': label})
        positions2d.append(i)

    names = cath_handler.get_names(labels)
    encoded_labels_names = dict(zip(encoded_labels, names))
    return {'positions2d': positions2d, 'mapping': encoded_labels_names}


def get_embedding(fold_space_vector=None):
    '''
    Compute the embedding in the space representation for a RCC vector.

    #Arguments
        fold_space_vector: array-like, RCC vector.

    #Returns
        A dictionary with keys: 'x', 'y', where x and y describes the embedding.
    '''
    space_reps = current_app.config.get('SPACE_REPS')
    cath_space = space_reps.get(current_app.config.get('CATH_STR'))

    embedding = cath_space.get_space_embedding([fold_space_vector])[0]
    return point(embedding)


def get_kdtree(handler=None):
    """
    Creates a instance of a KDTree given a handler for structure database using
    its elements described by its RCC vector.

    # Arguments
        handler: DataBaseHandler instance.

    # Returns
        A KDTree instance.

    """
    return KDTree(handler.get_features())
