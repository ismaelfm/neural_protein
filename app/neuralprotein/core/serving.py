import json

import numpy as np
import tensorflow as tf
from flask import current_app
from google.protobuf.json_format import MessageToJson
from tensorflow_serving.apis import predict_pb2

from fold_space import scale_fold_space_vectors

OUTPUT_STR = 'output'
INPUT_STR = 'input'


def _classification_aux(fsv=None,
                        database_name=None,
                        signature=None,
                        output_filter=[OUTPUT_STR]):
    request = predict_pb2.PredictRequest()
    request.model_spec.name = database_name
    request.model_spec.signature_name = signature

    X = scale_fold_space_vectors([fsv], database=database_name)

    request.inputs[INPUT_STR].CopyFrom(
        tf.contrib.util.make_tensor_proto(X, shape=X.shape, dtype=tf.float32))

    request.output_filter.extend(output_filter)

    return json.loads(
        MessageToJson(current_app.config['STUB'][database_name].Predict(
            request, current_app.config['TIMEOUT'])))['outputs']


def _format_probas(probas_message):
    dim = tuple([int(i['size']) for i in probas_message['tensorShape']['dim']])

    probas = np.asarray(probas_message['floatVal'])
    probas.resize(dim)

    return probas
