import json

import numpy as np
from flask import current_app

from serving import OUTPUT_STR, _classification_aux, _format_probas
from valid_functions import must_be_a_CATH_level


def _single_classification_aux(fsv=None, level_acronym=None):
    must_be_a_CATH_level(level_acronym)
    probas_message = _classification_aux(fsv,
                                         current_app.config.get('CATH_STR'),
                                         level_acronym)[OUTPUT_STR]
    return _format_probas(probas_message)


def get_classification(fsv=None, level_acronym=None):
    """
    Returns the CATH classification of the protein corresponding to the RCC
    vector specified.

    # Arguments
        fsv: array-like, RCC vector.
        level_acronym: str, level of classification, possible values: 'C', 'CA'
            'CAT' and 'CATH'.

    # Returns
        The label of the classification.
    """
    prediction = [_single_classification_aux(fsv, level_acronym).argmax()]

    handlers = current_app.config.get('HANDLERS')
    cath_handler = handlers.get(current_app.config.get('CATH_STR'))

    label = cath_handler.get_encoder(level_acronym).inverse_transform(
        prediction)
    return cath_handler.get_names(label)[0], label[0]


def get_classification_proba(fsv=None, level_acronym=None, n=4):
    """
    Returns the CATH classification probabilities and names of the n most
    likely classes to the protein corresponding to the the RCC vector specified.

    # Arguments
        fsv: array-like, RCC vector.
        level_acronym: str, level of classification, possible values: 'C', 'CA'
            'CAT' and 'CATH'.
        n: int, number of most likely classes to consider.

    # Returns
        List of probabilities-names.
    """
    probas = _single_classification_aux(fsv, level_acronym)[0]

    n_positions = probas.argsort()[-n:][::-1]

    handlers = current_app.config.get('HANDLERS')
    cath_handler = handlers.get(current_app.config.get('CATH_STR'))

    lec = cath_handler.get_encoder(level_acronym)
    labels = lec.inverse_transform(n_positions)

    return probas[n_positions].tolist(), cath_handler.get_names(
        labels), labels.tolist()


def get_all_classification_proba(fsv=None, n=4):
    """
    Returns the CATH classification probabilities and names of the n most
    likely classes to the protein corresponding to the the RCC vector specified.

    # Arguments
        fsv: array-like, RCC vector.
        level_acronym: str, level of classification, possible values: 'C', 'CA'
            'CAT' and 'CATH'.
        n: int, number of most likely classes to consider.

    # Returns
        List of probabilities-names.
    """
    message = _classification_aux(
        fsv,
        current_app.config.get('CATH_STR'),
        signature='ALL',
        output_filter=['C', 'CA', 'CAT'])
    all_probas = []
    for level in ('C', 'CA', 'CAT'):
        probas = _format_probas(message[level])[0]

        n_positions = probas.argsort()[-n:][::-1]

        handlers = current_app.config.get('HANDLERS')
        cath_handler = handlers.get(current_app.config.get('CATH_STR'))

        lec = cath_handler.get_encoder(level)
        labels = lec.inverse_transform(n_positions)

        all_probas.append({
            'probas': probas[n_positions].tolist(),
            'classNames': cath_handler.get_names(labels),
            'classIDs': labels.tolist(),
            'level': level
        })

    return all_probas
