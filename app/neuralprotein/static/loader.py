import cPickle as pickle
import os

import numpy as np
from flask import current_app


def load_scores(identifier):
    scores = np.load(
        os.path.join(current_app.config.get('DATA_PATH'), 'scores' + '.npz'))
    return scores[identifier]


def load_data(identifier):
    data_file = os.path.join(
        current_app.config.get('DATA_PATH'), identifier + '.pickle')

    with open(data_file, 'rb') as f:
        save = pickle.load(f)
        data = save[identifier]
        return data


def load_space(identifier):
    id_space = '%s%s' % (identifier, current_app.config.get('SPACE_STR'))
    return load_data(id_space)
