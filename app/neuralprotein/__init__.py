from flask import Flask
from flask_cors import CORS
from flask_pymongo import PyMongo

from static import loader

mongo_log = PyMongo()
mongo_data = PyMongo()
cors = CORS()


def create_app():

    app = Flask(__name__)
    app.config.from_pyfile('../config.py')

    mongo_log.init_app(app, config_prefix='MONGOLOG')
    mongo_data.init_app(app, config_prefix='MONGODATA')

    cors.init_app(app)

    from .api_1_0 import api as api_1_0_blueprint
    app.register_blueprint(api_1_0_blueprint, url_prefix='/api/v1')

    return app
