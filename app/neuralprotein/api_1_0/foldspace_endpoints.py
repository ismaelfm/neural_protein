from flask import jsonify, make_response, request
from webargs.flaskparser import use_kwargs

from . import api
from ..core.fold_space import (get_embedding, get_fold_space_vector,
                               get_fold_space_vectors,
                               get_cath_space_representation)
from ..core.neuralprotein_args import get_pdb_name, pdb_args, vector_args
from ..core.residue_interaction_graph import get_residue_interaction_graph_json
from ..logs import ip_log


@api.route("/pdb/residue_interaction_graph/", methods=['POST'])
@use_kwargs(pdb_args, locations=('json', 'form', 'query'))
@ip_log
def get_residue_interaction_graph_from_pdb(chain, segments, distance_cutoff,
                                           _gzip):
    pdb_filename = get_pdb_name(_gzip, request)

    residue_interaction_graph = get_residue_interaction_graph_json(
        pdb_filename, chain, segments, distance_cutoff)
    return make_response(
        jsonify({'residueInteractionGraph': residue_interaction_graph}), 200)


@api.route("/pdb/fold_space_vector/", methods=['POST'])
@use_kwargs(pdb_args, locations=('json', 'form', 'query'))
@ip_log
def get_fold_space_vector_from_pdb(chain, segments, distance_cutoff, _gzip):
    pdb_filename = get_pdb_name(_gzip, request)

    fsv = get_fold_space_vector(pdb_filename, chain, segments, distance_cutoff)
    return make_response(jsonify({'foldSpaceVector': fsv}), 200)


@api.route("/pdb/fold_space_vectors/", methods=['POST'])
@use_kwargs(pdb_args, locations=('json', 'form', 'query'))
@ip_log
def get_fold_space_vectors_from_pdb(chain, segments, distance_cutoff, _gzip):
    pdb_filename = get_pdb_name(_gzip, request)

    fsvs = get_fold_space_vectors(pdb_filename, segments, distance_cutoff)
    return make_response(jsonify({'foldSpaceVectors': fsvs}), 200)


@api.route("/space_representation/CATH/", methods=['GET'])
@ip_log
def cath_space_representation():
    space_representation = get_cath_space_representation()

    return make_response(jsonify(space_representation), 200)


@api.route("/vector/embedding2D/", methods=['POST'])
@use_kwargs(vector_args, locations=('json', 'form', 'query'))
@ip_log
def get_2d_embedding_from_vector(foldSpaceVector):

    embedding = get_embedding(foldSpaceVector)

    return make_response(jsonify({'embedding2D': embedding}), 200)


@api.route("/pdb/embedding2D/", methods=['POST'])
@use_kwargs(pdb_args, locations=('json', 'form', 'query'))
@ip_log
def get_2d_embedding_from_pdb(chain, segments, distance_cutoff, _gzip):
    pdb_filename = get_pdb_name(_gzip, request)

    fsv = get_fold_space_vector(pdb_filename, chain, segments, distance_cutoff)

    embedding = get_embedding(fsv)

    return make_response(jsonify({'embedding2D': embedding}), 200)
