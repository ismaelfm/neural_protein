from flask import jsonify, make_response, request
from webargs.flaskparser import use_kwargs

from . import api
from ..core.fold_space import get_fold_space_vector
from ..core.neuralprotein_args import get_pdb_name, pdb_args, vector_args
from ..core.scop import get_closest_domains, get_nearest_domains
from ..logs import ip_log


@api.route("/pdb/SCOP/closest_domains/lite/<int:n_neighbors>/",
           methods=['POST'])
@use_kwargs(pdb_args, locations=('json', 'form', 'query'))
@ip_log
def get_closest_domains_lite_from_pdb(chain, segments, distance_cutoff, _gzip,
                                      n_neighbors):
    pdb_filename = get_pdb_name(_gzip, request)

    fsv = get_fold_space_vector(pdb_filename, chain, segments, distance_cutoff)
    closest_domains = get_closest_domains(fsv, n_neighbors, False)
    return make_response(jsonify({'closestDomains': closest_domains}), 200)


@api.route("/pdb/SCOP/closest_domains/full/<int:n_neighbors>/",
           methods=['POST'])
@use_kwargs(pdb_args, locations=('json', 'form', 'query'))
@ip_log
def get_closest_domains_from_pdb(chain, segments, distance_cutoff, _gzip,
                                 n_neighbors):
    pdb_filename = get_pdb_name(_gzip, request)

    fsv = get_fold_space_vector(pdb_filename, chain, segments, distance_cutoff)
    closest_domains = get_closest_domains(fsv, n_neighbors, True)
    return make_response(jsonify({'closestDomains': closest_domains}), 200)


@api.route("/pdb/SCOP/nearest_domains/lite/<int:n_neighbors>/",
           methods=['POST'])
@use_kwargs(pdb_args, locations=('json', 'form', 'query'))
@ip_log
def get_nearest_domains_lite_from_pdb(chain, segments, distance_cutoff, _gzip,
                                      n_neighbors):
    pdb_filename = get_pdb_name(_gzip, request)

    fsv = get_fold_space_vector(pdb_filename, chain, segments, distance_cutoff)
    nearest_domains = get_nearest_domains(fsv, n_neighbors, False)
    return make_response(jsonify({'nearestDomains': nearest_domains}), 200)


@api.route("/pdb/SCOP/nearest_domains/full/<int:n_neighbors>/",
           methods=['POST'])
@use_kwargs(pdb_args, locations=('json', 'form', 'query'))
@ip_log
def get_nearest_domains_from_pdb(chain, segments, distance_cutoff, _gzip,
                                 n_neighbors):
    pdb_filename = get_pdb_name(_gzip, request)

    fsv = get_fold_space_vector(pdb_filename, chain, segments, distance_cutoff)
    nearest_domains = get_nearest_domains(fsv, n_neighbors, True)
    return make_response(jsonify({'nearestDomains': nearest_domains}), 200)


@api.route("/vector/SCOP/closest_domains/full/<int:n_neighbors>/",
           methods=['POST'])
@use_kwargs(vector_args, locations=('json', 'form', 'query'))
@ip_log
def get_closest_domains_from_vector(foldSpaceVector, n_neighbors):

    closest_domains = get_closest_domains(foldSpaceVector, n_neighbors, True)
    return make_response(jsonify({'closestDomains': closest_domains}), 200)


@api.route("/vector/SCOP/closest_domains/lite/<int:n_neighbors>/",
           methods=['POST'])
@use_kwargs(vector_args, locations=('json', 'form', 'query'))
@ip_log
def get_closest_domains_lite_from_vector(foldSpaceVector, n_neighbors):
    closest_domains = get_closest_domains(foldSpaceVector, n_neighbors, False)
    return make_response(jsonify({'closestDomains': closest_domains}), 200)


@api.route("/vector/SCOP/nearest_domains/full/<int:n_neighbors>/",
           methods=['POST'])
@use_kwargs(vector_args, locations=('json', 'form', 'query'))
@ip_log
def get_nearest_domains_full_from_vector(foldSpaceVector, n_neighbors):
    nearest_domains = get_nearest_domains(foldSpaceVector, n_neighbors, True)
    return make_response(jsonify({'nearestDomains': nearest_domains}), 200)


@api.route("/vector/SCOP/nearest_domains/lite/<int:n_neighbors>/",
           methods=['POST'])
@use_kwargs(vector_args, locations=('json', 'form', 'query'))
@ip_log
def get_nearest_domains_lite_from_vector(foldSpaceVector, n_neighbors):
    nearest_domains = get_nearest_domains(foldSpaceVector, n_neighbors, False)
    return make_response(jsonify({'nearestDomains': nearest_domains}), 200)
