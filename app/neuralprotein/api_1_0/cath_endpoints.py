from flask import jsonify, make_response, request
from webargs.flaskparser import use_kwargs

from . import api
from ..core.cath import (get_all_classification_proba, get_classification,
                         get_classification_proba)
from ..core.fold_space import get_fold_space_vector
from ..core.neuralprotein_args import get_pdb_name, pdb_args, vector_args
from ..logs import ip_log


@api.route(
    "/pdb/CATH/protein_structure_classification/<classification_level>/",
    methods=['POST'])
@use_kwargs(pdb_args, locations=('json', 'form', 'query'))
@ip_log
def get_cath_classification_from_pdb(chain, segments, distance_cutoff, _gzip,
                                     classification_level):
    pdb_filename = get_pdb_name(_gzip, request)

    fsv = get_fold_space_vector(pdb_filename, chain, segments, distance_cutoff)
    class_name, class_id = get_classification(fsv, classification_level)
    return make_response(
        jsonify({
            'className': class_name,
            'classID': class_id
        }), 200)


@api.route(
    "/pdb/CATH/protein_structure_classification/proba/<classification_level>/",
    methods=['POST'])
@use_kwargs(pdb_args, locations=('json', 'form', 'query'))
@ip_log
def get_cath_classification_proba_from_pdb(chain, segments, distance_cutoff,
                                           _gzip, classification_level):
    pdb_filename = get_pdb_name(_gzip, request)

    fsv = get_fold_space_vector(pdb_filename, chain, segments, distance_cutoff)
    probas, class_names, class_ids = get_classification_proba(
        fsv, classification_level)
    return make_response(
        jsonify({
            'probas': probas,
            'classNames': class_names,
            'classIDs': class_ids
        }), 200)


@api.route(
    "/vector/CATH/protein_structure_classification/proba/<classification_level>/",
    methods=['POST'])
@use_kwargs(vector_args, locations=('json', 'form', 'query'))
@ip_log
def get_cath_classification_proba_from_vector(foldSpaceVector,
                                              classification_level):
    probas, class_names, class_ids = get_classification_proba(
        foldSpaceVector, classification_level)
    return make_response(
        jsonify({
            'probas': probas,
            'classNames': class_names,
            'classIDs': class_ids
        }), 200)


@api.route(
    "/vector/CATH/protein_structure_classification/<classification_level>/",
    methods=['POST'])
@use_kwargs(vector_args, locations=('json', 'form', 'query'))
@ip_log
def get_cath_classification_from_vector(foldSpaceVector, classification_level):

    class_name, class_id = get_classification(foldSpaceVector,
                                              classification_level)
    return make_response(
        jsonify({
            'className': class_name,
            'classID': class_id
        }), 200)


@api.route(
    "/vector/CATH/protein_structure_classification/proba/", methods=['POST'])
@use_kwargs(vector_args, locations=('json', 'form', 'query'))
@ip_log
def get_cath_classification_data_from_vector(foldSpaceVector):
    return make_response(
        jsonify({
            'data': get_all_classification_proba(foldSpaceVector)
        }), 200)
