from flask import Blueprint

api = Blueprint('api', __name__)

from . import main, foldspace_endpoints, cath_endpoints, scop_endpoints
