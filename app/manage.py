import logging
import os

from flask import current_app
from grpc.beta import implementations
from tensorflow_serving.apis import prediction_service_pb2

import setup as st
from flask_script import Manager
from neuralprotein import create_app
from neuralprotein.core.fold_space import get_kdtree
from neuralprotein.logs import ip_log
from neuralprotein.static import loader
from rcc.RCCTools import structure_databases

app = create_app()


@app.before_first_request
def before_first_request():
    app.config['HANDLERS'] = {
        x: loader.load_data(x)
        for x in app.config.get('REGISTERED_HANDLERS')
    }
    app.config['SPACE_REPS'] = {
        x: loader.load_space(x)
        for x in app.config.get('REGISTERED_SPACES')
    }
    app.config['KDTREES'] = {
        x: get_kdtree(app.config.get('HANDLERS').get(x))
        for x in app.config.get('REGISTERED_KDTREES')
    }

    app.config['STUB'] = {
        'cath':
        prediction_service_pb2.beta_create_PredictionService_stub(
            implementations.insecure_channel(app.config[
                'CATH_SERVICE_HOST'], int(app.config['CATH_SERVICE_PORT']))),
        'scop':
        prediction_service_pb2.beta_create_PredictionService_stub(
            implementations.insecure_channel(app.config[
                'SCOP_SERVICE_HOST'], int(app.config['SCOP_SERVICE_PORT'])))
    }
    app.config['SCORES'] = {
        x: loader.load_scores(x)
        for x in app.config.get('REGISTERED_SCORES')
    }


@app.route('/')
@ip_log
def index():
    return 'Welcome to Neural Protein!\nPlease visit docs.neuralprotein.space'


manager = Manager(app)


@manager.command
def setup(force=False):
    "Initial server setup."
    logging.info('\nNeural Protein data setup:\n')

    st.test_dir(current_app.config.get('UPLOAD_FOLDER'))
    st.test_dir(current_app.config.get('DATA_PATH'))

    cath_handler = st.test_handler(
        current_app.config.get('CATH_STR'),
        structure_databases.CATHHandler,
        current_app.config.get('CATH_CONFIG'),
        init=force)
    scop_handler = st.test_handler(
        current_app.config.get('SCOP_STR'),
        structure_databases.SCOPHandler,
        current_app.config.get('SCOP_CONFIG'),
        init=force)

    st.test_space_representation(
        current_app.config.get('CATH_STR'),
        cath_handler,
        current_app.config.get('CATH_SPACE_REPRESENTATION'),
        current_app.config.get('CATH_SPACE_LABEL'),
        current_app.config.get('N_REPRESENTANTS'),
        init=force)

    logging.info('\nNeural Protein models setup:\n')

    st.test_superfamily_scores()

    logging.info('\nNeural Protein setup complete.')


if __name__ == '__main__':
    manager.run()
