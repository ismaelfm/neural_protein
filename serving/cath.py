"""
Export the CATH model.

Usage: exportCATH.py [--model_version=y] export_dir
"""

import os
import sys

import tensorflow as tf
from keras import backend as K
from keras.models import load_model, model_from_json
from tensorflow.python.saved_model import builder as saved_model_builder
from tensorflow.python.saved_model import (signature_constants,
                                           signature_def_utils, tag_constants,
                                           utils)
from tensorflow.python.util import compat

from utils import get_input_signature_rcc, rebuild_model

tf.app.flags.DEFINE_integer('model_version', 1, 'version number of the model.')
FLAGS = tf.app.flags.FLAGS


def main(_):
    if len(sys.argv) != 3:
        print('Usage: exportCATH.py model_file export_dir')
        sys.exit(-1)
    if FLAGS.model_version <= 0:
        print 'Please specify a positive value for version number.'
        sys.exit(-1)

    sess = tf.Session()
    K.set_session(sess)
    print sys.argv[-2]

    model = rebuild_model(sys.argv[-2])

    export_path = os.path.join(
        compat.as_bytes(sys.argv[-1]),
        compat.as_bytes(str(FLAGS.model_version)))

    print 'Exporting trained model to', export_path
    builder = saved_model_builder.SavedModelBuilder(export_path)
    # Build the signature_def_map.

    inputs = get_input_signature_rcc(model)

    prediction_signature_C = signature_def_utils.build_signature_def(
        inputs=inputs,
        outputs={
            'output':
            utils.build_tensor_info(model.get_layer('dense_308').output)
        },
        method_name=signature_constants.PREDICT_METHOD_NAME)

    prediction_signature_CA = signature_def_utils.build_signature_def(
        inputs=inputs,
        outputs={
            'output':
            utils.build_tensor_info(model.get_layer('dense_4').output)
        },
        method_name=signature_constants.PREDICT_METHOD_NAME)

    prediction_signature_CAT = signature_def_utils.build_signature_def(
        inputs=inputs,
        outputs={
            'output':
            utils.build_tensor_info(model.get_layer('dense_30').output)
        },
        method_name=signature_constants.PREDICT_METHOD_NAME)

    prediction_signature_ALL = signature_def_utils.build_signature_def(
        inputs=inputs,
        outputs={
            'C': utils.build_tensor_info(model.get_layer('dense_308').output),
            'CA': utils.build_tensor_info(model.get_layer('dense_4').output),
            'CAT': utils.build_tensor_info(model.get_layer('dense_30').output)
        },
        method_name=signature_constants.PREDICT_METHOD_NAME)

    legacy_init_op = tf.group(
        tf.initialize_all_tables(), name='legacy_init_op')

    builder.add_meta_graph_and_variables(
        sess, [tag_constants.SERVING],
        signature_def_map={
            'C': prediction_signature_C,
            'CA': prediction_signature_CA,
            'CAT': prediction_signature_CAT,
            'ALL': prediction_signature_ALL
        },
        legacy_init_op=legacy_init_op)

    builder.save()

    print 'Done exporting!'


if __name__ == '__main__':
    tf.app.run()
