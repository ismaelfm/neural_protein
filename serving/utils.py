from keras import backend as K
from keras.models import load_model, model_from_json
from tensorflow.python.saved_model import utils


# re-build a model where the learning phase is now hard-coded to 0
def rebuild_model(model_fn):
    model = load_model(model_fn)
    config = model.to_json()
    weights = model.get_weights()

    K.set_learning_phase(0)

    new_model = model_from_json(config)
    new_model.set_weights(weights)

    del model

    return new_model


def get_input_signature_rcc(model):
    return {'input': utils.build_tensor_info(model.input)}
