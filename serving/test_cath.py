"""Manual test client for tensorflow_model_server."""

import numpy as np
import tensorflow as tf
from grpc.beta import implementations
from sklearn.preprocessing import scale
from tensorflow.core.framework import types_pb2
from tensorflow.python.platform import flags
from tensorflow_serving.apis import predict_pb2, prediction_service_pb2

#tf.app.flags.DEFINE_string('server',
#                           'https://safe-waters-76905.herokuapp.com/:8500',
#                           'inception_inference service host:port')
FLAGS = tf.app.flags.FLAGS


def main(_):
    # Prepare request
    request = predict_pb2.PredictRequest()
    request.model_spec.name = 'cath'
    request.model_spec.signature_name = 'C'
    request.inputs['input'].CopyFrom(
        tf.contrib.util.make_tensor_proto(
            [[
                11., 1., 11., 9., 18., 8., 48., 3., 2., 1., 19., 2., 1., 28.,
                6., 0., 0., 0., 0., 0., 0., 0., 0., 1., 1., 0.
            ]],
            shape=[1, 26]))
    request.output_filter.append('output')
    # Send request
    #host, port = FLAGS.server.split(':')
    channel = implementations.insecure_channel('localhost', int(8500))
    stub = prediction_service_pb2.beta_create_PredictionService_stub(channel)

    #print stub.Predict(request, 5.0)
    print stub.Predict(request, 5.0)
    #prediction_dim = tuple([i.size for i in response_values.tensor_shape.dim])
    #scores = np.asarray(response_values.float_val)  #.resize(prediction_dim)
    #scores.resize(prediction_dim)
    #print scores


if __name__ == '__main__':
    tf.app.run()
