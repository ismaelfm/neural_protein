"""Manual test client for tensorflow_model_server."""

import json

import numpy as np
import tensorflow as tf
from google.protobuf.json_format import MessageToJson
from grpc.beta import implementations
from sklearn.preprocessing import scale
from tensorflow.core.framework import types_pb2
from tensorflow.python.platform import flags
from tensorflow_serving.apis import predict_pb2, prediction_service_pb2

#tf.app.flags.DEFINE_string('server',
#                           'https://safe-waters-76905.herokuapp.com/:8500',
#                           'inception_inference service host:port')
FLAGS = tf.app.flags.FLAGS


def main(_):
    # Prepare request
    request = predict_pb2.PredictRequest()
    request.model_spec.name = 'scop'
    request.model_spec.signature_name = 'superfamily'
    request.inputs['input'].CopyFrom(
        tf.contrib.util.make_tensor_proto(
            [[
                -0.21794993, -0.61566072, -0.94586185, -0.40516295, 0.06399959,
                -1.07929551, -1.06062469, -0.39686566, -0.39354686,
                -0.33170839, -1.02901794, -1.17277429, -0.2434689, -1.00684459,
                0.97850731, -0.09759071, -0.13126848, -0.25813749, -0.1891772,
                -0.34748309, -0.5841202, 0.46170026, -0.17680268, 0.42103706,
                -0.88040152, 0.52655658
            ]],
            shape=[1, 26]))
    request.output_filter.append('output')
    # Send request
    #host, port = FLAGS.server.split(':')
    channel = implementations.insecure_channel('localhost', int(8501))
    stub = prediction_service_pb2.beta_create_PredictionService_stub(channel)

    #print stub.Predict(request, 5.0)
    probas_message = json.loads(
        MessageToJson(stub.Predict(request, 5.0)))['outputs']['output']
    dim = tuple([int(i['size']) for i in probas_message['tensorShape']['dim']])

    probas = np.asarray(probas_message['floatVal'])
    probas.resize(dim)
    print probas.argmax()
    #prediction_dim = tuple([i.size for i in response_values.tensor_shape.dim])
    #scores = np.asarray(response_values.float_val)  #.resize(prediction_dim)
    #scores.resize(prediction_dim)
    #print scores


if __name__ == '__main__':
    tf.app.run()
